package telegram

import (
	objs "github.com/SakoDroid/telego/objects"
)

func StartHandler(u *objs.Update) {
	bot.SendMessage(
		u.Message.Chat.Id,
		"Welcome to the Tsatsubii bot",
		"",
		0,
		false,
		false,
	)
}
