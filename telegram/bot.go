package telegram

import (
	"fmt"
	"os"

	bt "github.com/SakoDroid/telego"
	cfg "github.com/SakoDroid/telego/configs"
	objs "github.com/SakoDroid/telego/objects"
	"gitlab.com/tsatsubii/tsatsubii-service"
)

var bot *bt.Bot

func StartBot() {
	var err error

	tsatsubii.EnsureEnv("TELEGRAM_API_KEY")

	up := cfg.DefaultUpdateConfigs()
	cf := cfg.BotConfigs{
		BotAPI:         cfg.DefaultBotAPI,
		APIKey:         os.Getenv("TELEGRAM_API_KEY"),
		UpdateConfigs:  up,
		Webhook:        false,
		LogFileAddress: cfg.DefaultLogFile,
	}

	bot, err = bt.NewBot(&cf)
	if err != nil {
		return
	}

	err = bot.Run()
	if err != nil {
		return
	}

	go func() {
		updateChannel := bot.GetUpdateChannel()

		bot.AddHandler("/id", func(u *objs.Update) {
			msg := fmt.Sprintf("%d", u.Message.Chat.Id)
			bot.SendMessage(
				u.Message.Chat.Id,
				msg,
				"",
				u.Message.MessageId,
				false,
				false,
			)
		}, "private")

		for {
			update := <-*updateChannel

			bot.SendMessage(
				update.Message.Chat.Id,
				"Unknown command",
				"",
				update.Message.MessageId,
				false,
				false,
			)
		}
	}()
}

func SendMessage(msg string, users []int) {
	fmt.Println(users)
	for _, user := range users {
		fmt.Printf("SENDMESSAGE: to %d\n", user)
		_, err := bot.SendMessage(
			user,
			msg,
			"",
			0,
			false,
			false,
		)
		if err != nil {
			fmt.Println(err)
		}
	}
}
