package main

import (
	"time"

	"gitlab.com/tsatsubii/tsatsubii-service"

	"gitlab.com/tsatsubii/tsatsubii-telegram/telegram"
)

func main() {
	tsatsubii.Init()
	telegram.StartBot()
	tsatsubii.Start()
}
